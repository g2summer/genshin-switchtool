# 原神切服器

#### 介绍
24.11.14 以前初学py写着玩的 已放弃维护
原神切换服务器，支持官服转换为b服

#### 软件架构
软件架构说明
x64Python

#### 安装教程

1.  转到Release页面
2.  下载文件
3.  解压至任意目录
4.  启动“Start-Tool.exe”

#### 使用说明

1.  需要启动器目录
2.  支持版本为当前原神版本
3.  更新直接下载新的release然后覆盖安装即可

#### 参与贡献

1.  Fork 本仓库
2.  新建issue反馈软件存在的BUG
3.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
