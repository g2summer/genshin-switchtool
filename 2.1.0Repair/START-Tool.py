import os

vers = '2.1.0'
print("·软件版本:"+str(vers)+" 正在检测更新...")
try:
    url = ('https://gitee.com/summer-uefi/software-problem-feedback/raw/master/verinfo.html')
    resp = urllib.request.urlopen(url)  # 向浏览器发送请求
except:
    print("T_T网络连接错误")
    checkupdate()

else:
    vs = resp.read().decode('utf-8')
    if vers in vs:
        print("·未检测到更新！")
    else:
        print("·发现版本更新！版本号：" + vs + "正在自动更新")
        os.system("start updatestart.exe")
time.sleep(1.5)
clear()

print("输入T或t进入软件。输入其他字符退出。")
t= input("请输入：")
if(t == 'T' or t == 't'):
    pth = os.getcwd()
    qd = 'start ' + pth + '/main/tool.exe'
    os.system(qd)